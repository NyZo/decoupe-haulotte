<style>
/* FONT */
@font-face {
	font-family: swis;
	src: url('fonts/swiss721bt-roman-condensed.eot?#iefix') format('embedded-opentype'),
		 url('fonts/swiss721bt-roman-condensed.woff') format('woff'), 
		 url('fonts/swiss721bt-roman-condensed.ttf')  format('truetype'), 
		 url('fonts/swiss721bt-roman-condensed.svg#swiss721bt-roman-condensed') format('svg');
	font-weight: normal;
	font-style: normal;
}
@font-face {
	font-family: cent;
	src: url('fonts/century.eot?#iefix') format('embedded-opentype'),  
		 url('fonts/century.woff') format('woff'),
		 url('fonts/century.ttf')  format('truetype'), 
		 url('fonts/century.svg#century') format('svg');
	font-weight: normal;
	font-style: normal;
  }

/* RESET */
body, ul, li, ol, form, h1, h2, h3, h4, h5, h6, div, span, p     { padding:0; margin:0; border:0; -webkit-text-size-adjust:none; -moz-text-size-adjust:none; text-size-adjust:none;}
article, aside, footer, header, main, nav, section               { display:block;}  
input, textarea, select { -webkit-appearance:none; -ms-appearance:none; appearance:none; -moz-appearance:none; -o-appearance:none; border-radius:0;}
strong,b,em				{ font-weight:normal; font-family:Arial; font-style: normal;}
ul						{ list-style-type:none;}
body					{ font:normal 14px/24px Arial; color:#555555; background:#fff;}
a						{ text-decoration:none; color:#1e8930; outline:none;}
img						{ border:none;}
#wrapper 				{ min-width:320px; overflow:hidden; position:relative;}
#wrapper *				{ box-sizing:border-box; outline:none;}
.wrap					{ max-width:1050px; width:100%; margin:0 auto; padding:0; position:relative;} 

/* CLASS */
.clear			{ clear:both; float:none !important; width:100% !important; padding:0 !important; margin:0 !important; display:block;}
.left			{ float:left;}
.right 			{ float:right;}
.titre, 
.titrePage		{ margin-bottom: 10px; padding-bottom: 17px; position: relative; font:normal 20px/24px cent; text-transform:uppercase; color:#000;}
.titrePage 		{ margin-bottom: 15px;}
.titre:after  	{ content:''; background: #1e8930; width: 30px; height: 2px; position: absolute; bottom: 0; left: 0; right: 0; margin: 0 auto;}
.titrePage:after{ content:''; background: #1e8930; width: 30px; height: 2px; position: absolute; bottom: 0; left: 0;}
.chapo 			{ font: normal 14px/24px Arial; color: #555555;}
.s-Titre 		{ margin: 30px 0; font: normal 17px/24px Arial; color: #1e8930;}
.sousTitre 		{ font: normal 20px/25px Arial; color: #000; margin:0 0 20px;}
.link 			{ display:inline-block; background:#1e8930; width:auto; height:60px; padding:18px 28px 0 28px; position:relative; font:normal 14px/24px Arial; color:#fff; text-decoration:none !important;}
.clr:after 		{ content:''; display:table; clear:both;}

/* HEADER */
#header						{ width:100%; position:relative;}
.headerTop					{ width:100%; position:relative; z-index:100;}
.headerTop .logo			{ display:block; position:absolute; left:0; top:0; margin:45px 0 0; z-index:65; color: #999999; line-height: 18px;}
.logo img, .logo strong 	{ display: inline-block; vertical-align: sub;}
.logo strong 				{ margin-left: 10px;}
.logo span 					{ display: block; font: normal 24px/24px cent; color: #000; text-transform: uppercase;}

.tel-slog 					{ background: rgba(0,0,0,.8); width: 100%; height: 60px; position: absolute; bottom: 0; left: 0; right: 0; z-index: 75;}
.slogan 					{ background:url(images/icone-slogan.svg) 0 50% no-repeat; height:60px; padding:20px 0 0 45px; font: 14px/18px Arial; color:#fff;}
.tel-slog .tel				{ background:url(images/icone-tel.svg) 100% 50% no-repeat; width:auto; height:60px; padding:18px 35px 0 0; font-size:14px; color:#fff;}
.tel-slog .tel a 			{ color: #fff;}

/* MENU */
.menu						{ width:100%; display:block; position:relative; z-index:60;}	
.menu>ul					{ width:100%; display:block; position:relative; text-align:right; background:#fff; font-size:0; line-height:0; }	
.menu>ul>li					{ display:inline-block; position:relative; padding:0 19px;}
.menu>ul>li:after 			{ content: ''; width: 1px; height: 25px; border-right:1px #e5e5e5 solid; position: absolute; right: 0; top: 18px; bottom: 0; margin: auto;}
.menu>ul>li:nth-child(6) 	{ padding-right: 0 !important;}
.menu>ul>li:nth-child(6):after{ display: none;}
.menu>ul>li>a:after 		{ content: ''; background:#1e8930; width: 0; height: 6px; position:absolute; left: 0; right: 0; bottom: 0;}
.menu a						{ display:block; width:auto; height:145px; padding:0; position:relative; font:normal 16px/167px swis; color:#000; text-transform: uppercase;}
.menu>ul>li.active>a:after  { content: ''; background:#1e8930; width: 100%; height: 6px; position:absolute; left: 0; right: 0; bottom: 0;}

/* SUB */
.sub						{ width:300px; position:absolute; top:145px; left:50%; z-index:995; margin-left:-150px; display:block; background:#1e8930; visibility:hidden; opacity:0;}
.sub li						{ width:100%; margin:0; border:none; padding:0; text-align:center;}
.sub li a					{ display:block; width:100%; height:60px; font:normal 14px/60px Arial; color:#fff; position:relative; border-bottom:1px solid #fff; margin:0;}
.sub li:last-child a		{ border:0;}
.menu li:hover .sub 		{ opacity:1; visibility:visible; z-index:999;}
.menu .sub li.active 		{ background:#fff;}
.menu .sub li.active a		{ color:#1e8930;}
.menu .social 				{ display:none; width:100%; padding:16px 40px; border-bottom:1px #000 solid;}
.menu .social a 			{ display:block; width:45px; height:45px; padding:0; margin-right:8px; text-indent:-9999px; cursor:pointer; border:none;}
.social .facebook			{ background:url(images/icone-facebook2.svg) 50% no-repeat #000;}

/* HIDE MOBILE */
.wrapMenuMobile, .menu .vueMobile{ display:none;}

/* RESPONSIVE */
	@media (min-width:1201px) { 
		.menu ul { display:block !important;}
	}
	@media (max-width:1200px) {
		.wrap 								{ padding: 0 40px;}
		.headerTop.wrap 					{ max-width: 100%;}
		.wrapMenuMobile						{ background: #000; width:50%; height:45px; display:block; z-index:80; color:#fff; font:normal 15px/22px Arial; cursor:pointer; padding:14px 20px 0 40px; text-transform:uppercase; position:absolute; left:0; top:0; z-index:105;}
		.menuMobile							{ width:20px; display:block; height:18px; padding-left:35px; cursor:pointer; position:relative; line-height:20px;}
		.menuMobile>div						{ width:20px; height:2px; background:#fff; position:absolute; left:0; top:50%; margin-top:-1px;}
		.menuMobile>div:before				{ width:100%; height:2px; background:#fff; position:absolute; left:0; top:6px; content:"";}
		.menuMobile>div:after				{ width:100%; height:2px; background:#fff; position:absolute; left:0; top:-6px; content:"";}
		.menuMobile.active>div				{ height:0px;}
		.menuMobile.active>div:before		{ top:0; transform:rotate(45deg);}
		.menuMobile.active>div:after		{ top:0; transform:rotate(-45deg);}
		.menu 								{ width:100%; height:0; position: absolute; left: 0; z-index: 100;}
		.menu>ul 							{ width:100%; height:auto; position:absolute; left:0; top:0; padding:0; background:#fff; z-index:999; display:none;}
		.menu>ul:after 						{ content: ""; width: 100%; height: 9999px; background: rgba(0,0,0,.6); position: absolute; left: 0; bottom: -9999px; z-index: -1;}
		.menu ul li 						{ width:100%; margin:0 auto; display:block; float:none; padding:0;}
		.menu>ul>li:after 					{ display: none;}
		.menu>ul>li>a						{ height:53px; line-height:53px; border:0; padding:0 40px; margin:0; font-size:16px; text-align:left; border-bottom:1px solid #000;}
		.menu>ul>li.active>a 				{ color:#1e8930;}
		.menu > ul > li.active > a span 	{ display: none;}
		.menu i 							{ display:block; width:100%; height:53px; position:absolute; right:0; top:0;}
		.menu i:before 						{ display:block; width:16px; height:8px; position:absolute; right:40px; top:50%; margin-top:-5px; content:""; background:url(images/icone-sub-menu.svg) 50% no-repeat; transform:rotate(180deg);}
		.menu i:after 						{ display:none; width:16px; height:8px; position:absolute; right:40px; top:50%; margin-top:-5px; content:""; background:url(images/icone-sub-menu.svg) 50% no-repeat;}
		.menu i.active:before, .menu li.active i:before{ display:none;}
		.menu i.active:after, .menu li.active i:after{ display:block;}
		.menu>ul>li.active>a:after 			{ display: none;}
		.menu .vueMobile 					{ display:block;}
		.menu .sub 							{ display:none; visibility:visible; width:100%; position:relative; left:auto; top:auto; margin:0; opacity:1; background:#ececec; padding:0; border-top:none;}
		.menu .sub li a						{ border-bottom:1px #000 solid; font-size:16px; color:#000; text-align:left; padding:0 40px; font-family: swis;}
		.menu .sub li.active a				{ background:#1e8930; color: #fff;}
		.menu .social 						{ display: block;}
		.headerTop 							{ height: 195px; padding: 45px 40px 0 !important; text-align: center;}
		.headerTop .logo 					{ display: inline-block; width: 355px; margin: 47px auto; left: 0; top: 45px; right: 0;}
		.headerTop .logo strong 			{ text-align: left !important;}
		.tel-slog 							{ background: #1e8930;	width: 50%;	height: 45px; bottom: inherit; left: inherit; right: 0; top: 0; z-index: 100;}
		.slogan, .tel-slog .tel em 			{ display: none;}
		.tel-slog .tel 						{ background: none; height: inherit; padding: 0;}
		.tel-slog .tel a 					{ display: block; background: url(images/icone-tel2.svg) 0 50% no-repeat; padding: 10px 0 0 30px; height: 45px; font: normal 16px/24px swis}
	}
	@media (max-width:600px) {
		.wrap 								{ padding: 0 20px;}
		.headerTop 							{ padding: 45px 20px 0 !important;}
		#slider 							{ height:250px; min-height: 250px;}	
		.txt-banner 						{ padding: 0 40px; font: normal 24px/34px cent;}
		.wrapMenuMobile						{ padding:14px 20px 0 20px;}
		.menu > ul > li > a 				{ padding:0 20px;}
		.menu .sub li a 					{ height: inherit; padding: 16px 20px; line-height: 24px;}
		.menu i::before, .menu i::after		{ right:20px;}
		.menu .social 						{ padding: 16px 20px;}
	}
	@media (max-width:400px) {
		.headerTop .logo 					{ width: 100%;}
		.headerTop .logo img 				{ width: 32px;}
		.headerTop .logo strong				{ font-size: 10px; line-height: 14px;}
		.logo span 							{ font: normal 18px/18px cent;}
	}

/* Homepage */
<?php if ($namePage == "pageAccueil") { ?>
	/* BANNER */
	#slider						{ width:100%; height:660px; position:relative; z-index:45;}
	#slider .slick-list			{ width:100%; height:100%;}
	#slider .slick-track		{ width:100%; height:100%;}
	#slider .banner1			{ background:url(images/banner1-1800.jpg) 50% 50% no-repeat; -webkit-background-size:cover !important; -moz-background-size:cover !important; background-size:cover !important;}
	#slider .banner2			{ background:url(images/banner2-1800.jpg) 50% 50% no-repeat; -webkit-background-size:cover !important; -moz-background-size:cover !important; background-size:cover !important;}
	#slider .banner3			{ background:url(images/banner3-1800.jpg) 50% 50% no-repeat; -webkit-background-size:cover !important; -moz-background-size:cover !important; background-size:cover !important;}
	.txt-banner					{ position:absolute; left: 0; right:0; top:50%; z-index:50; margin-top:25px; font: normal 36px/40px cent; color:#fff; text-transform: uppercase; text-align: center;text-shadow: -1px 2px 2px rgba(0, 0, 0, 1);}
	.txt-banner em 				{ display: block; font: normal 36px/40px cent; color:#fff; text-transform: uppercase;}

	.blocIntro 					{ padding: 90px 0 100px; text-align: center;}
	.blocIntro .link 			{ margin-top: 35px;}

	.blocInfo 					{ background: url(images/bg-texture.jpg) repeat; padding: 75px 0; text-align: right;}
	.blocInfo .left 			{ width: 300px; height: 450px; position: absolute; left: 0; top: -105px; z-index: 50;}
	.blocInfo .left:before 		{ content: ''; width: px; height: 390px; position: absolute; left: -10px; top: 0; bottom: 0;}
	.blocInfo .right:before 	{ content: ''; width: px; height: 390px; position: absolute; right: -10px; top: 0; bottom: 0;}
	.blocInfo .right 			{ padding-left: 460px;}
	.blocInfo .titre:after 		{ left: inherit; right: 0 !important;}
	.blocInfo .titre, .blocInfo p{ color: #fff;}
	.blocInfo .right .link 		{ margin-top: 30px;}

	.blocMatiere 				{ padding: 90px 0; text-align: center;}
	.blocMatiere>ul 			{ padding: 0; margin: 35px 0 0; font-size: 0; line-height: 0;}
	.blocMatiere>ul>li 			{ display: inline-block; vertical-align: middle; width: 33%; margin: 1px; text-align: left !important;}
	.blocMatiere>ul>li>a 		{ display: block; background:url(images/icone-arrow.svg) 80% 50% no-repeat #e5e5e5; padding:18px 0 18px 30px; font: normal 14px/24px Arial; color:#555555;}

	.blocExpertise 				{ background: url(images/bg-texture.jpg) repeat; height: 150px; padding: 60px 0 0; text-align: center;}
	.blocExpertise span 		{ display: inline-block; margin: 0 40px; padding: 0 30px; position: relative; font: normal 20px/25px cent; color: #fff; text-transform: uppercase;}
	.blocExpertise span:before 	{ content: '“'; position: absolute; left: 0; top: -10px; font: normal 48px/25px cent; color: #1e8930;}
	.blocExpertise span:after 	{ content: '”'; position: absolute; right: 0; top: -10px; font: normal 48px/25px cent; color: #1e8930;}

	.blocFaq 					{ padding: 100px 0;}
	.blocFaq .left 				{ background: rgba(255,255,255,.7); width: 525px; padding-bottom: 40px; position: absolute; top: 0; left: 0; z-index: 50;}
	.blocFaq .left .titre:after { right: inherit !important;}
	.blocFaq .left .link 		{ margin-top: 35px;}

/* RESPONSIVE */
	@media (max-width:1200px) {
		.wrap 								{ padding: 0 40px;}
		#slider 							{ height:450px; min-height:450px;}
		#slider .banner1					{ background:url(images/banner1-1200.jpg) 50% 50% no-repeat;}
		#slider .banner2					{ background:url(images/banner2-1200.jpg) 50% 50% no-repeat;}
		#slider .banner3					{ background:url(images/banner3-1200.jpg) 50% 50% no-repeat;}
		.txt-banner 						{ margin-top: 50px;}
		
		.blocIntro 							{ padding: 75px 40px 80px;}
		.blocInfo 							{ padding: 75px 0 70px 40px;}
		.blocInfo .right 					{ padding-left: 370px;}
		.blocMatiere 						{ padding: 65px 0 70px;}
		.blocMatiere p 						{ padding-left:40px; padding-right: 40px;}
		.blocExpertise 						{ padding:60px 0 0;}
		.blocFaq 							{ padding: 70px 0;}
		.blocFaq .left 						{ left: 0;}
		.blocFaq .left 						{ background: none; width: 100%; position: relative; top: 0; left: 0; padding:0; text-align: center;}
		.blocFaq .left .titre::after 		{ right: 0 !important;}
		.blocFaq .right						{ display: none;}
	}
	@media (max-width:1024px) {
		.blocInfo .right 					{ padding-left: 340px;}
		.blocMatiere>ul>li>a>em 			{ display: block;}
	}
	@media (max-width:980px) {
		.blocInfo 							{ padding: 0; text-align: left;}
		.blocInfo .left 					{ position: relative; top: 0; width: 36%; float: none; display: inline-block; vertical-align: middle;}
		.blocInfo .right 					{ display: inline-block; vertical-align: middle; float: none; width: 53%; margin:0 0 0 10%; padding: 75px 0 70px; text-align: right !important;}
	}
	@media (max-width:760px) {		
		#slider .banner1					{ background:url(images/banner1-800.jpg) 0 no-repeat;}
		#slider .banner2					{ background:url(images/banner2-800.jpg) 0 no-repeat;}
		#slider .banner3					{ background:url(images/banner3-800.jpg) 0 no-repeat;}

		.blocInfo .left 					{ display: none;}
		.blocInfo .right 					{ margin: 0; width: 100%; padding: 75px 0 70px;}
		.blocMatiere>ul>li 					{ width: 49.6%;}
		.blocMatiere>ul>li>a>em 			{ display: inline-block;}
		.blocMatiere>ul>li:nth-child(5) a em,
		.blocMatiere>ul>li:nth-child(6) a em{ display: block;}
	}
	@media (max-width:600px) {
		.wrap 								{ padding: 0 20px;}
		#slider 							{ background:url(images/banner1-800.jpg) 0 no-repeat; height:250px; min-height: 250px;}
		.txt-banner 						{ padding: 0 40px; font: normal 24px/34px cent;}
		.txt-banner em 						{ font: normal 24px/34px cent;}
		
		.blocIntro 							{ padding: 75px 20px 80px;}
		.titre 								{ font: normal 18px/24px cent;}
		.blocMatiere 						{ padding: 60px 0 0;}
		.blocMatiere p 						{ padding-left:20px; padding-right: 20px;}
		.blocMatiere > ul > li 				{ display: block; width: 100%; margin: 1px 0;}
		.blocMatiere > ul > li > a 			{ padding: 18px 0 18px 20px;}
		.blocMatiere>ul>li:nth-child(5) a em, 
		.blocMatiere>ul>li:nth-child(6) a em{ display: inline;}
		.blocMatiere>ul>li:last-child>a { background:url(images/icone-arrow-hov.svg) 93% 50% no-repeat #1e8930; color: #fff;}
		.blocExpertise						{ display: none;}
	}
	@media (max-width:400px) {
		.txt-banner 						{ padding: 0 60px;}
	} 
<?php } ?>

/* Pages Intérieures */
<?php if ($namePage != "pageAccueil") { ?>
	/* BANNER */
	#slider						{ background:url(images/banner-page-1800.jpg) 50% 50% no-repeat; -webkit-background-size:cover !important; -moz-background-size:cover !important; background-size:cover !important; width:100%; height:400px; position:relative; z-index:45;}
	.txt-banner					{ position:absolute; left: 0; right:0; top:50%; z-index:50; margin-top:25px; font: normal 36px/40px cent; color:#fff; text-transform: uppercase; text-align: center; text-shadow: -1px 2px 2px rgba(0, 0, 0, 1);}
	.txt-banner em 				{ font: normal 36px/40px cent; color:#fff; text-transform: uppercase;}

	/* Navigation*/
	.Breadcrumb 				{ margin: 0 0 35px;}
	.Breadcrumb a 				{ color: #555555; position: relative;}
	.Breadcrumb .active 		{ color:#1e8930;}
	.Breadcrumb a:first-of-type:after{ content: ''; background: #555555; width: 4px; height: 4px; border-radius: 50%; position: relative; display: inline-block; vertical-align: middle; margin: 0 5px 2px 10px;}

	.pageContent				{ width:100%; position:relative; padding:0 0 0 340px;}
	.pageRight					{ width:100%; max-width:920px; position:relative; margin:0 auto; padding:50px 60px 60px;}
	.pageContent p				{ margin:0 0 15px 0;}
	.pageContent ul				{ margin:35px 0; padding: 0;}
	.pageRight ul li			{ margin: 0; padding:14px 0 14px 40px; border-top:1px #e5e5e5 solid; line-height:23px; letter-spacing:.3px; background:url(images/icone-liste.svg) 7px 17px no-repeat; position:relative;}
	.pageRight ul li:last-child { border-bottom:1px #e5e5e5 solid;}
	.blocPhoto					{ width:100%; max-width:670px; height:auto; margin:0 auto; padding:20px 0 5px;}
	.blocPhoto img				{ display: block; width:100%; height:auto; margin:0; padding:0;}

	.aside						{ background:url(images/bg-texture.jpg) repeat; width:340px; height:auto; position:absolute; left:0; top:0; text-align: center;}
	.sous-aside 				{ padding: 70px 40px;}
	.aside .titre, .aside p 	{ color: #fff;}
	.sous-aside .link 			{ margin-top: 20px;}
	.decouvre 					{ display: block; background:url(images/icone-arrow-hov.svg) 93% 50% no-repeat #1e8930; height: 60px; margin:0; padding:18px 0 0 20px; color: #fff; text-align: left;}
	
/* RESPONSIVE */
	@media (max-width:1200px) { 
		#slider							{ background:url(images/banner-page-1200.jpg) 50% 50% no-repeat; height: 250px; min-height: 250px;}
		.txt-banner 					{ margin-top: 70px; padding: 0 40px;}
		.pageRight						{ padding:50px 40px 60px;}
	}
	@media (max-width:1100px) { 
		.pageContent					{ padding:0; min-height:0;}
		.aside							{ display:none;}
	}
	@media (max-width:760px) { 
		#slider							{ background:url(images/banner-page-800.jpg) 0 no-repeat;}
	}
	@media (max-width:600px) { 
		.txt-banner 					{ margin-top: 55px;}
		.txt-banner, .txt-banner  em 	{ font: normal 24px/34px cent;}
		.pageRight						{ padding:50px 20px;}
		.pageRight .chapo span,
		.pageRight .s-Titre span,
		.pageRight p span, 
		.pageRight li span 				{ display: none;}

	}
	@media (max-width:400px) {
		.txt-banner 					{ padding: 0 60px;}
	} 
<?php } ?>

/* FOOTER */
#footer						{ background:url(images/bg-texture.jpg) repeat; width:100%; height:auto; position:relative;}
.footer 					{ padding:80px 0 55px;}
#footer div 				{ color:#fff;}
.footer1 .logo-foot			{ display:block; font:normal 20px/24px cent; color:#fff; text-transform:uppercase;}
.footer1 span 				{ display: block; margin-top: 5px; font-family: swis; color: #7c7c7c; line-height: 22px; letter-spacing:.3px;}
.open-plan 					{ display:block; margin-top:15px; font: normal 14px/22px swis; cursor:pointer; color:#1e8930; letter-spacing:.3px;}

.footer2					{ margin-left:95px; font: normal 14px/22px swis; color:#fff; letter-spacing: .3px;}
.footer2 li 				{ display:block; margin-bottom: 20px; padding:0 0 0 40px; position:relative;}
.footer2 li:first-child 	{ background:url(images/icone-plan.svg) 3px 50% no-repeat;}
.footer2 li:nth-child(2) 	{ background:url(images/icone-tel.svg) 0 50% no-repeat;}
.footer2 li:last-child 		{ background:url(images/icone-lettre.svg) 0 49% no-repeat;}
.footer2 a					{ color:#fff;}

.footer3 					{ margin-left: 120px;}
.footer3 .rs				{ display:block; cursor:pointer;}
.footer3 a 					{ display:block; float:left; margin:0 8px 0 0; cursor:pointer; color: #fff;}
.footer3 .rs.facebook		{ background:url(images/icone-facebook.svg) 0 40% no-repeat; font-family: swis; padding-left: 40px;}

.toponweb					{ display:block; width: 120px; position:fixed; right:0; bottom:-65px; z-index:85; transition:bottom 400ms ease-in-out;}
.toponweb span				{ width:auto; height:60px; display:block; padding:20px; background:#1e8930;}
.toponweb img				{ width:74px; height:auto; display:block; margin:0 auto;}
.toponweb.show				{ bottom:-4px;}
.plan						{ width:100%; position:relative; padding:16px 60px; background:#000; text-align:center; overflow:hidden; display:none;}
.plan a						{ padding:0 8px; color:#fff;}

/* EFFET HOVER */
@media (min-width:1201px) {
	body a span, body a, span:before, span:after, a:after, a:before, .link, .slick-prev, .slick-next, .slick-dots button, .menu>ul>li>a:after, .sub, .blocMatiere>ul>li>a { -webkit-transition:all 400ms ease-in-out; -moz-transition:all 400ms ease-in-out; -ms-transition:all 400ms ease-in-out; transition:all 400ms ease-in-out;}
	a:hover								{ color:#1e8930;}
	.logo:hover							{ opacity:.5;}
	.tel-slog a:hover 					{ color:#1e8930;}
	.menu>ul>li:hover>a:after 			{ content: ''; background:#1e8930; width: 100%; height: 6px; position:absolute; left: 0; right: 0; bottom: 0;}
	.menu .sub a:hover 					{ background:#fff; color:#1e8930;}
	.txt-banner a:hover:after			{ width:0%;}
	.scroll:hover						{ opacity:.5;}
	.link:hover 						{ background-color:#000; color:#fff;}
	.blocMatiere>ul>li:hover>a 			{ background:url(images/icone-arrow-hov.svg) 93% 50% no-repeat #1e8930; color: #fff;}
	.liste4:hover>a 					{ background:url(images/icone-arrow-hov.svg) 97% 50% no-repeat #1e8930 !important;}
	/*.blocInfo .right .link:hover,
	.sous-aside .link:hover 			{ background-color: #fff; color:#1e8930;}*/
	.decouvre:hover 					{  background:url(images/icone-arrow-hov.svg) 97% 50% no-repeat #1e8930; color:#fff;}
	.hide .link:hover					{ background-color:#fff; color:#1e8930;}
	.slick-prev:hover 					{ background-color:#1e8930;}
	.slick-next:hover 					{ background-color:#1e8930;}
	.footer .toponweb:hover span		{ background:#24a139;}
	.open-plan:hover 					{ color: #fff;}
}

/* RESPONSIVE */
@media (max-width:1200px) {
	.footer 							{ max-width:1130px; padding:75px 40px;}
	.plan 								{ padding: 16px 40px 75px;}
}
@media (max-width:1024px) {
	.footer1, .footer2, .footer3 		{ margin-left: 0; padding-right: 50px;}
	.footer1 							{ margin-bottom: 50px;}
}
@media (max-width:760px) {
	.footer 							{ padding:75px 40px 130px;}
	.footer>div 						{ float:none; padding: 0;}
	.footer1 							{ width:100%; margin-bottom: 0; text-align: left;}
	.footer2, .footer3 					{ width: 255px; margin: 0 auto;}
	.footer2 							{ margin: 40px 0 0;}
	.footer3 							{ margin-left: 0;}
}
@media (max-width:600px) {
	.footer 							{ padding:75px 20px 130px;}
	.plan 								{ padding: 16px 20px 75px;}
	.open-plan 							{ margin-top:23px;}
	.footer2 li							{ margin-bottom: 18px;}
}

/* SLICK */
.slick-slider					{ position:relative; display:block; -moz-box-sizing:border-box; box-sizing:border-box; -webkit-user-select:none; -moz-user-select:none; -ms-user-select:none; user-select:none; -webkit-touch-callout:none; -khtml-user-select:none; -ms-touch-action:pan-y; touch-action:pan-y; -webkit-tap-highlight-color:transparent;}
.slick-list						{ position:relative; display:block; overflow:hidden; margin:0; padding:0;}
.slick-list:focus				{ outline:none;}
.slick-list.dragging			{ cursor:pointer; cursor:hand;}
.slick-slider .slick-track,
.slick-slider .slick-list		{ -webkit-transform:translate3d(0 0,0); -moz-transform:translate3d(0,0,0); -ms-transform:translate3d(0,0,0); -o-transform:translate3d(0,0,0); transform:translate3d(0,0,0);}
.slick-prev 					{ width:50px; height:70px; background:url(images/icone-arrow-left.svg) 46% 50% no-repeat #fff; position:absolute; left:0; top:50%; z-index:50; cursor:pointer; text-indent:-9999px; outline:none; border:0; padding:0; margin-top:-35px;}
.slick-next 					{ width:50px; height:70px; background:url(images/icone-arrow-right.svg) 56% 50% no-repeat #fff; position:absolute; right:0; top:50%; z-index:50; cursor:pointer; text-indent:-9999px; outline:none; border:0; padding:0; margin-top:-35px;}
.slick-dots						{ width:100%; height:10px; margin-top:-37px; position:relative; text-align:center; padding:0; line-height:0; z-index:50;}
.slick-dots	li					{ display:inline-block; height:10px; margin:0 10px;}
.slick-dots button				{ background:#b2b2b2; display:block; width:10px; height:10px; border-radius:50%; text-indent:-9999px;outline:none; padding:0;border:1px solid #b2b2b2; cursor:pointer;}
.slick-active button			{ background:#373a8c; border:1px solid #373a8c;}
.slick-track					{ position:relative; top:0; left:0; display:block;}
.slick-track:before, 
.slick-track:after 				{ display:table; content: '';}
.slick-track:after				{ clear:both;}
.slick-loading .slick-track		{ visibility:hidden;}
.slick-slide					{ display:none; float:left; height:100%; min-height:1px;}
[dir='rtl'] .slick-slide		{ float:right;}
.slick-slide img				{ display:block;}
.slick-slide.slick-loading img	{ display:none;}
.slick-slide.dragging img		{ pointer-events:none;}
.slick-initialized .slick-slide	{ display:block;}
.slick-loading .slick-slide		{ visibility:hidden;}
.slick-vertical .slick-slide 	{ display:block; height:auto; border:1px solid transparent;}
</style>