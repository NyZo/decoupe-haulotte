<?php $namePage="pageAccueil"; $nameSub="pageNone"; ?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Homepage | Haulotte & Associés</title>
        <meta name="description" content="Homepage | Haulotte & Associés" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
        <!--[if lt IE 9]>
        <script src="js/html5.js"></script>
        <![endif]-->
        
        <?php include "css/css.php";?>
    </head>
    
    <body>
        <div id="wrapper">
            <?php include "header.php";?>
            <main id="homepage">
                <div class="blocIntro wrap">
                    <h2 class="titre">Votre avocat à Waterloo</h2>
                    <p>Proin gravida nibh vel <a href="#" title="velit auctor aliquet">velit auctor aliquet</a>. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit conse quat ipsum, nec sagittis sem nibh.Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum aunsequat ipsum, nec sagittctor, nisi elit <a href="#" title="consequat ipsum">consequat ipsum</a>, nec sagittis sem nibh.</p>
                    <a href="#" class="link" title="En savoir plus">En savoir plus</a>
                </div>
                <div class="blocInfo">
                    <div class="wrap clr">
                        <div class="left">
                            <img src="images/portrait.jpg" alt="">
                        </div>
                        <div class="right">
                            <h2 class="titre">Haulotte & Associés</h2>
                            <p>Proin gravida <a href="#" title="nibh vel velit">nibh vel velit</a> auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh.Proin gravida <a href="#" title="nibh vel velit auctor aliquet">nibh vel velit auctor aliquet</a>. Aenean sollicittudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh.Proin grudin, lorem quis bibendum aunsequat.</p>
                            <a href="#" class="link" title="En savoir plus">En savoir plus</a>
                        </div>
                    </div>
                </div>
                <div class="blocMatiere wrap">
                    <h2 class="titre">Nos matières</h2>
                    <p>Proin gravida nibh vel velit <a href"#" title="auctor aliquet">auctor aliquet</a>. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh.Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum aunsequat ipsum, nec sagittctor, <a href="#" title="nisi elit consequat">nisi elit consequat</a> ipsum, nec sagittis sem nibh.</p>
                    <ul>
                        <li><a href="#" title="Circulation routière">Circulation routière</a></li>
                        <li><a href="#" title="Réparation du préjudice corporel">Réparation du préjudice corporel</a></li>
                        <li><a href="#" title="Récupération de créances">Récupération de créances</a></li>
                        <li><a href="#" title="Baux résidentiels et commerciaux">Baux résidentiels<em> et commerciaux</em></a></li>
                        <li class="liste4"><a href="#" title="Droit des assurances et de la responsabilité">Droit des assurances<em> et de la responsabilité</em></a></li>
                        <li><a href="#" title="Droit de l’immobilier et de la construction">Droit de l’immobilier<em> et de la construction</em></a></li>
                        <li><a href="#" title="Copropriété">Copropriété</a></li>
                        <li><a href="#" title="Conflit de voisinage">Conflit de voisinage</a></li>
                        <li><a href="#" title="Droit pénal général">Droit pénal général</a></li>
                        <li><a href="#" title="Divorce">Divorce</a></li>
                        <li><a href="#" title="Droit familial et patrimonial">Droit familial et patrimonial</a></li>
                        <li><a href="#" title="Découvrir nos matières">Découvrir nos matières</a></li>
                    </ul>
                </div>
                <div class="blocExpertise wrap">
                    <span>Une expertise professionnelle au service des clients…</span>
                </div>
                <div class="blocFaq">
                    <div class="wrap clr">
                        <div class="left">
                            <h2 class="titre">FAQ ou liens utiles</h2>
                            <p>Proin gravida nibh vel velit <a href="#" title="auctor aliquet">auctor aliquet</a>. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh.Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, <a href="#" title="lorem quis bibendum aunsequat">lorem quis bibendum aunsequat</a> ipsum, nec sagittctor, nisi elit consequat ipsum, nec sagittis sem nibh.</p>
                            <a href="#" class="link" title="En savoir plus">En savoir plus</a>
                        </div>
                        <div class="right">
                            <img src="images/img-faq.jpg" alt="">
                        </div>
                    </div>
                </div>
            </main>
            <?php include "footer.php";?>
        </div>
		<script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/slick.min.js"></script>
        <script type="text/javascript" src="js/custom.js"></script>
        <script>
			$(document).ready(function() {	
				// SLIDER
				w=$(window).width()
				if (w<601){
					if($('#slider').hasClass('slick-initialized')){
						$('#slider').slick("unslick");
					 }
				}
				else{
					isInit=$('#slider').hasClass('slick-initialized')
					if(!isInit){
						$('#slider').slick({
							fade:true
						});
				   }
				};				
			});
        </script>
    </body>
</html>