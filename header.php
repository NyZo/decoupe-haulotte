<!-- HEADER START -->
<header id="header" <?php if ($namePage != "pageAccueil") echo("class='page'"); ?>>
    <div class="headerTop wrap clr">
        <a href="homepage.php" class="logo" title="Haulotte & Associés">
            <img src="images/icone-logo.svg" alt="Haulotte & Associés">
            <strong>
                <span>Haulotte & Associés</span>Cabinet d’avocats à Waterloo
            </strong>
        </a>
        <div class="wrapMenuMobile"><div class="menuMobile"><span>MENU</span><div></div></div></div>
        <nav class="menu">
            <ul>
                <li <?php if ($namePage == "pageAccueil") echo (" class='active'"); ?>><a href="homepage.php" title="Accueil">Accueil</a></li>
                <li <?php if ($namePage == "page...") echo (" class='active'"); ?>><a href="page.php" title="Cabinet">Cabinet</a></li>
                <li <?php if ($namePage == "pageCon") echo (" class='active'"); ?>><a href="#" title="Matières">Matières</a><i></i>
                    <ul class="sub">
                        <li class="vueMobile<?php if ($namePage == "pageCon" && $nameSub == "pageNone") echo (" active"); ?>"><a href="#" title="Vue d'ensemble">Vue d'ensemble</a></li>
                        <li <?php if ($nameSub == "pageCon1") echo (" class='active'"); ?>><a href="#" title="Page">Page</a></li>
                        <li <?php if ($nameSub == "pageCon1") echo (" class='active'"); ?> class="active"><a href="" title="Page">Page</a></li>
                        <li <?php if ($nameSub == "pageCon1") echo (" class='active'"); ?>><a href="#" title="Page">Page</a></li>
                        <li <?php if ($nameSub == "pageCon1") echo (" class='active'"); ?>><a href="#" title="Page">Page</a></li>
                    </ul>
                </li>
                <li <?php if ($namePage == "pageCon1") echo (" class='active'"); ?>><a href="#" title="Liens utiles">Liens utiles</a></li>
                <li <?php if ($namePage == "pageCon1") echo (" class='active'"); ?>><a href="#" title="Faq">Faq</a></li>
                <li <?php if ($namePage == "pageCon1") echo (" class='active'"); ?>><a href="#" title="Contact">Contact</a></li>
                <li class="social clr">
                    <a title="Rejoignez-nous sur Facebook" target="_blank" class="facebook rs">Facebook</a>
                </li>
            </ul>
        </nav>
    </div>    
    <div class="tel-slog">
        <div class="wrap clr">
            <?php if ($namePage == "pageAccueil") { ?>
                <h1 class="slogan left">Avocat inscrit au Barreau du Brabant wallon depuis 1991</h1>
            <?php } else { ?>
                <div class="slogan left">Avocat inscrit au Barreau du Brabant wallon depuis 1991</div>
            <?php } ?>
            <span class="tel right"><em>Nous contacter : <a href="tel:23546963" title="02/354.69.63">02/354.69.63</a> - </em><a href="tel:475474768" title="0475/47.47.68">0475/47.47.68</a></span> 
        </div>   
    </div>
    <div id="slider">
        <div class="banner1"></div>
        <div class="banner2"></div>
        <div class="banner3"></div>
    </div>
    <div class="txt-banner">
        Défendre vos intérêts <em>et vos droits</em>
    </div>    
</header>
<!-- HEADER END -->