$(document).ready(function() {	
	// MENU MOBILE //
	$(".wrapMenuMobile").click(function() {
		$(this).toggleClass('active');
		$(".menuMobile").toggleClass('active');
		$(".menu ul").fadeToggle();
		$(".sub").css('display','none');
		$(".menu li i").removeClass('active');
	});	
	$(".menu li i").click(function() {
		$(this).toggleClass('active');
		$(".menu").find('.sub' ).slideUp();
		if($(this).hasClass("active")){
			$(".menu li i").removeClass('active')
			$(this).next().slideToggle();
			$(this).toggleClass('active');
		}
	});		

	// FOOTER //
	$(".open-plan").click(function() {
		$(".plan").slideToggle()
		$("html, body").animate({scrollTop: $(document).height()}, "slow");
	});	
	
	// SCROLL //
	$(".scroll").click(function() {
		var c = $(this).attr("href")
		$('html, body').animate({ scrollTop: $("#" + c).offset().top }, 1000, "linear");
		return false
	})
	
	// scroll logo
	 $(window).scroll(function(){
		var posScroll = $(document).scrollTop();
		if(posScroll>160) 
			$('.toponweb').addClass('show')
		else
			$('.toponweb').removeClass('show')
	});
});