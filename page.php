<?php $namePage="page..."; $nameSub="pageNone"; ?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Page | Haulotte & Associés</title>
        <meta name="description" content="Page | Haulotte & Associés" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
        <!--[if lt IE 9]>
        <script src="js/html5.js"></script>
        <![endif]-->
        
        <?php include "css/css.php";?>
    </head>
    
    <body>
    	<div id="wrapper">
			<?php include "header.php";?>
			
			<main class="pageContent">
                <div class="pageRight">
					<div class="blocTop">
						<div class="Breadcrumb">
							<a href="homepage.php" title="Accueil">Accueil</a> <span class="active">Cabinet</span>
						</div>
						<h1 class="titrePage">Cabinet</h1>
						<h2 class="chapo">Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit conse quat ipsum<span>, nec sagittis sem nibh.Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum aunsequat ipsum, nec sagittctor, nisi elit consequat ipsum, nec sagittis sem nibh</span>.</h2>
					</div>	
					<div class="blocText">
						<div class="s-Titre">Proin gravida nibh vel velit auctor<span> aliquetnean</span></div>
						<p>Gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum aunsequat ipsum, nec sagittctor<span>, nisi elit consequat ipsum, nec sagittis sem nibhibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum aunsequat ipsum, nec sagittctor, nisi elit consequat ipsum, nec sagittis sem nibh</span>.</p>
					</div>
					<div class="blocPhoto"><img src="images/img-page.jpg" alt="Haulotte & Associés" /></div>
					<div class="blocText">
						<div class="s-Titre">Nibh vel velit auctor aliquetneanvel<span> velit auctor alique</span></div>
						<p>Gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum aunsequat ipsum, nec sagittctor<span>, nisi elit consequat ipsum, nec sagittis sem nibhibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum aunsequat ipsum, nec sagittctor, nisi elit consequat ipsum, nec sagittis sem nibh</span>.</p>
						<ul>
							<li>Lorem quis bibendum aunsequat<span> ipsum gravida nibh vel velit auctor aliquet</span></li>
							<li>Sollicitudin, lorem quis bibendum<span> aunsequat ipsum. Gravida nibh vel velit auctor aliquet.</span></li>
							<li>Velit auctor aliquet<span>. A</span>enean sollicitud<span>in, lorem quis bibendum.</span></li>
							<li>Gravida nibh vel velit auctor aliquet<span>. Aenean sollicitudin, lorem quis bibendum aunsequat ipsum.</span></li>
						</ul>
						<p>Nec sagittctor, nisi elit consequat ipsum, nec sagittis sem nibhibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum aunsequat ipsum, nec sagittctor, nisi elit.</p>
					</div>
                </div>
                <?php include "aside.php";?>
            </main>
            
            <?php include "footer.php";?>
        </div>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/slick.min.js"></script>
        <script type="text/javascript" src="js/custom.js"></script>
    </body>
</html>