<!-- FOOTER START -->
<footer id="footer">
    <div class="footer wrap clr">
        <div class="footer1 left">
            <a href="homepage.php" class="logo-foot" title="Haulotte & associés">Haulotte & associés</a>
            <span>BE0837.438.414</span>
            <a class="open-plan" title="Plan du site">Plan du site</a>
        </div>          
        <div class="footer2 left">
            <ul>
                <li>Avenue des lilas, 14 - 1410 Waterloo</li>
                <li><a href="tel:23546963" title="02/354.69.63">02/354.69.63</a> - <a href="tel:475474768" title="0475/47.47.68">0475/47.47.68</a></li>
                <li>E-mail : <a href="mailto:compta.haulotte@gmail.com" title="compta.haulotte@gmail.com">compta.haulotte@gmail.com</a></li>
            </ul>
        </div>          
        <div class="footer3 left clr">
            <a title="Nous suivre sur FACEBOOK" target="_blank" class="facebook rs">Nous suivre sur FACEBOOK</a>
        </div>
        <a href="http://www.toponweb.be/" title="Réalisé par Toponweb" target="_blank" class="toponweb">
            <span><img src="images/toponweb.svg" alt="Top On Web" /></span>
        </a>
    </div>
    <div class="plan">
        <a href="#" title="Lorem Ipsum">Lorem Ipsum</a>
        <a href="#" title="Lorem Ipsum">Lorem Ipsum</a>
        <a href="#" title="Lorem Ipsum">Lorem Ipsum</a>
        <a href="#" title="Lorem Ipsum">Lorem Ipsum</a>
        <a href="#" title="Lorem Ipsum">Lorem Ipsum</a>
    </div>
</footer>    
<!-- FOOTER END -->